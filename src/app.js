// load the promises library q
var Q = require('q');

// load the template renderer handlebars
var Handlebars = require('handlebars');

// the gui object, created by Node-webkit
var gui = require('nw.gui');

// Utility functions for javascript
var _ = require('lodash');

// To load the steps into the program
var stepLoader = require('steploader');

var Inspector = new Backbone.Marionette.Application();

// Attach the application to the window object.
window.Inspector = Inspector;

/*
 * Create a namespace for the application
 */
_.extend(Inspector, {
    View: {},
    Model: {},
    Collection: {},
    TransformationModel: {},
    StepModels: {},
    Layout: {}
});

/*
 * Used to load the initial views
 */
Inspector.addRegions({
    Window: '.main-window'
});

Handlebars.registerHelper("debug", function(optionalValue) {
  console.log("Current Context");
  console.log("====================");
  console.log(this);
 
  if (optionalValue) {
    console.log("Value");
    console.log("====================");
    console.log(optionalValue);
  }
});

/*
 * Set the default render enging of the application
 * to handlebars
 */
Backbone.Marionette.Renderer.render = function (template, data) {
    return Handlebars.compile($(template).html())(data);
};


/* 
 * Utility function to prepare all templates for usage.
 */
function initTemplates() {
    // Load in external templates
    var ts = [];
    _.each(document.querySelectorAll('[type="text/x-template"]'), function (el) {
        var d = Q.defer();
        $.get(el.src, function (res) {
            el.innerHTML = res;
            d.resolve(true);
        });
        ts.push(d.promise);
    });

    return Q.all(ts);
}

/*
 * Utility function to update all the steps required to model an experiment
 */ 
function updateSteps() {
    var d = Q.defer();
    stepLoader.getModels().then(function (models) {
        for (var i = 0; i < models.length; i++) {
            var defaults = models[i];
            delete defaults._id;
            var model = Inspector.StepModel.extend({
                description: defaults
            });
            Inspector.StepModels[defaults.name] = model;

        }
        d.resolve();
    });

    return d.promise;
}


/*
 * Initializer, run when the application starts.
 *
 * - initializes the templates used in the views
 * - updates the StepCatalog
 * - finally starts the application by initializing the controller, router
 *   and initial view.
 */

Inspector.addInitializer(function () {
    console.log("nra");
    initTemplates()
        .then(updateSteps)
        .then(function () {
            var main = new Inspector.Layout.MainLayout();

            var controller = new Inspector.Controller({
                layout: main
            });
            var router = new Inspector.Router({
                controller: controller
            });
            Inspector.Window.show(main);
            Backbone.history.start();
           
        });

});