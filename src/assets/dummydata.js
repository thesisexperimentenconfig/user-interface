module.exports = [{
    rank: 1,
    sequence: 'GVIVESA',
    Q1: 690.38,
    Q3: 656.36,
    RT: 34.3,
    probability: 0.94,
    intensity: 0.32
            }, {
    rank: 2,
    sequence: 'ATL',
    Q1: 804.92,
    Q3: 1132.59,
    RT: 34.4,
    probability: 0.89,
    intensity: 0.28
            }, {
    rank: 3,
    sequence: 'EYVSTNR',
    Q1: 804.92,
    Q3: 868.42,
    RT: 34.4,
    probability: 0.88,
    intensity: 0.3
}];