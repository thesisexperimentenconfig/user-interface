var fs = require('fs');
var fileHandler = require('filehandler');
(function (Inspector) {
    Inspector.Controller = Backbone.Marionette.Controller.extend({

        /**
         * Initialize the controller
         * @param {Object} options Should contain the main layout for the application
         */
        initialize: function (options) {
            this.layout = options.layout;
            
            this.currentView = 'modellingLayout';
            this.views = {};
            this.viewStates = {};
        },

        /**
         * Method called when the application starts
         */
        home: function () {
            if(!!this.viewStates[this.currentView]){
                this.viewStates[this.currentView] = this.views[this.currentView].getViewState();
            }     
            if(!this.views.modellingLayout){
                this.views.modellingLayout = new Inspector.Layout.ModellingLayout();
            }
            this.layout.Main.show(this.views.modellingLayout, { preventDestroy: true });
            this.currentView = 'modellingLayout';
        },
        
        /**
         * Creates a new modelling environment
         */
        createNewModel: function () {
            this.viewStates[this.currentView] = this.views[this.currentView].getViewState();
            this.views.modellingLayout = new Inspector.Layout.ModellingLayout();
            this.layout.Main.show(this.views.modellingLayout, { preventDestroy: true });
            this.currentView = 'modellingLayout';
        },
        
        /**
         * Shows the current modelling environment
         */
        showCurrentModel: function(){
            this.viewStates[this.currentView] = this.views[this.currentView].getViewState();
            this.layout.Main.show(this.views.modellingLayout, { preventDestroy: true });
            
            if(this.viewStates.modellingLayout){
                var viewState = this.viewStates.modellingLayout;
                this.views.modellingLayout.restoreViewState(viewState);
            }
            this.currentView = 'modellingLayout';
        },
        
        /**
         * Opens an experiment from file.
         */
        openModel: function(){
            
            this.viewStates[this.currentView] = this.views[this.currentView].getViewState();
            var self = this;
            
            $('body').append('<input type="file" id="fileModel" accept=".simp" style="display:none">');
            $("#fileModel").click();
            $("#fileModel").on("change", function(){
                var filename = $("#fileModel").get(0).files[0].path;
                var data = fileHandler.openExperimentFile(filename);
                data = JSON.parse(data);
                self.views.modellingLayout.openVisualisation(data);
                
            });
            this.currentView = 'modellingLayout';
        },
        
        /**
         * Saves the current experiment to file.
         */
        saveModel: function(){
            var that = this;
            var filesave = $("#filesave");
            filesave.change(function(){
                var filename = $("#filesave").get(0).files[0].path;
                var visualisation = that.views.modellingLayout.saveVisualisation();
                
                fileHandler.makeExperimentFile(JSON.stringify(visualisation), filename);
            });
            filesave.click();
            
        },
        
        /**
         * shows the current mass spectra visualisation
         */
        currentMassSpectra: function(){
            this.viewStates[this.currentView] = this.views[this.currentView].getViewState();
            this.views.spectraView =  new Inspector.View.SpectraView();
            this.layout.Main.show(this.views.spectraView , { preventDestroy: true });
            if(this.viewStates.spectraView){
                this.views.spectraView.restoreViewState(this.viewStates.spectraView);
                this.currentView = 'spectraView';
            }
            
            
        },

        /**
         * Opens a new mass spectra visualisation van file
         */
        openMassSpectra: function () {
            var self = this;
            this.viewStates[this.currentView] = this.views[this.currentView].getViewState();
            
            $('body').append('<input type="file" id="openSpectra" style="display:none">');
            $("#openSpectra").click();
            $("#openSpectra").on("change", function(){
                 var filename = $("#openSpectra").get(0).files[0].path;
                self.views.spectraView =  new Inspector.View.SpectraView();
                self.layout.Main.show(self.views.spectraView , { preventDestroy: true });
                self.views.spectraView.openFile(filename);
                $('a[href="#currentMassSpectra"]').show();
                self.currentView = 'spectraView';
            });
           
        },
        
        /**
         * Show the connection tool
         */
        showPipeline: function(){
            this.viewStates[this.currentView] = this.views[this.currentView].getViewState();
            
            this.views.pipelineView = new Inspector.View.PipelineView();
            this.layout.Main.show(this.views.pipelineView , { preventDestroy: true });
            if(this.viewStates.pipelineView){
                this.views.pipelineView.restoreViewState(this.viewStates.pipelineView);
            }
            
            this.currentView = 'pipelineView';
        }
    });
})(window.Inspector);