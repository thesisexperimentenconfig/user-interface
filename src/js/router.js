(function(Inspector){
    Inspector.Router = Backbone.Marionette.AppRouter.extend({
        
        appRoutes:{
            "":  'home',
            "model": 'createNewModel',
            "openmodel": 'openModel',
            "savemodel": "saveModel",
            "currentmodel": "showCurrentModel",
            "openMassSpectra": 'openMassSpectra',
            'currentMassSpectra': 'currentMassSpectra',
            "pipeline": 'showPipeline',
            
        },
        
    });
})(window.Inspector);