(function(Inspector){
    /*
     * Collection used to store all steps used in an experiment.
     */
    Inspector.Collection.StepCollection = Backbone.Collection.extend({
        model: Inspector.StepModel,
        
        getXMLRepresentation: function(){
            var xml = "<devices>";
            this.each(function(model){
                 xml += model.getXMLRepresentation();
             })
            xml += "</devices>";
            
            return xml;
        }
    })
})(window.Inspector);