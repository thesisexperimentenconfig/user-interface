(function(Inspector){
    /*
     * Collection to store all connections between Models in the experiment
     */
    Inspector.Collection.ConnectionCollection = Backbone.Collection.extend({
        model: Inspector.Model.ConnectionModel,
        
        getXMLRepresentation: function(){
            var xml = "<connections>";
             this.each(function(model){
                 xml += model.getXMLRepresentation();
             })
            xml += "</connections>";
            
            return xml;
        }
    })
})(window.Inspector);