(function(Inspector){
    
    /*
     * Collection used to store all sequence to be analyzed by SIMPOPE.
     */
    Inspector.Collection.SequenceCollection = Backbone.Collection.extend({
        model: Inspector.Model.SequenceModel,
    });
    
})(window.Inspector);