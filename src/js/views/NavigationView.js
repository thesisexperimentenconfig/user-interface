var os = require('os');

(function (Inspector) {

    var win8x, win8y, win8h, win8w;

    Inspector.View.NavigationView  = Backbone.Marionette.ItemView.extend({
        template: "#navigation-tpl",
        id: "header",


        events: {
            'click #minimizeWindow': 'minimize',
            'click #maximizeWindow': 'maximize',
            'click #closeWindow': 'close',
            'click #model': "fire",
            'click a': 'changeFocus'
        },
        
        initialize: function () {
            this.win = require('nw.gui').Window.get();
            this.win.isMaximized = true;
            this.win.maximize();
            var _this = this;

            this.win.on('maximize', function () {
                _this.win.isMaximized = true;
            });

            this.win.on('unmaximize', function () {
                _this.win.isMaximized = false;
            });
            
            Mousetrap.bind('f12', function(){
                _this.win.showDevTools();
            });
        },

        /**
         * minimizes the app the app.
         */
        minimize: function () {
            this.win.minimize();
        },

        /**
         * Maximizes the app.
         */
        maximize: function () {
            if (this.win.isMaximized) {
                this.win.unmaximize();
            } else {
                this.win.maximize();
            }
        },

        /**
         * Closes the application
         */
        close: function () {
            this.win.close();
        }

    });

})(window.Inspector);