(function(Inspector){
    'use strict';
    /**
     * Layout used to model an experiment. Can contain a configurationView and a GraphView
     */
    Inspector.Layout.ModellingLayout = Backbone.Marionette.LayoutView.extend({
        
        template: "#modelling-tpl",
        id: "modelling",  
        
        regions:{
            Graph: "#graph-area",
            Config: "#config"
            
        },
        
        /**
         * Method that runs when this layout has been added to the application
         */
        onShow: function(){
            this.graphView = new Inspector.View.GraphView({
                parent: this
            });
            
            try{
                this.Graph.show(this.graphView);
                this.graphView.render();
            } catch(e){
                console.error("The GraphView could not be loaded, message: " + e);
            }
            
            
        },
        
        /**
         * Shows a configurationView in the Config Region.
         * @param {ConfigurationView} view The view to show.
         */
        showInConfig: function (view){
            if(this.configView){
                this.configView.destroy();
            }
            this.configView = view;
            this.Config.show(view, {preventDestroy: false});
        },
        
        saveModel: function(){
            var xml = this.graphView.getXMLRepresentation();
            return xml;
        },
        
        /**
         * Proxy method to save the visualisation for the graphView
         * @returns {Object} JSON representation of the experiment.
         */
        saveVisualisation: function(){
            var visualisation = this.graphView.saveVisualisation();
            return visualisation;
        },
        
        /**
         * Method to load a new GraphView from a json representation
         * @param {[[Type]]} file [[Description]]
         */
        openVisualisation: function(file){
            this.GraphView = new Inspector.View.GraphView({
                parent: this
            });
            

            this.Graph.show(this.graphView);
            this.graphView.render();
            this.graphView.loadGraphFromJSON(file);
        },
        
        /**
          * Retrieves the view state of the view.
          */
        getViewState: function(){
            var viewState = this.graphView.getViewState();
            return viewState;
            
        },
        
        /**
          * Restores the viewstate of the view.
          * @param {object} viewState 
          */
        restoreViewState: function(viewState){
            this.graphView.restoreViewState(viewState);
        }
        
        
    });

})(window.Inspector);