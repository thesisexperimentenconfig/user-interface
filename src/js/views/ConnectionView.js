(function (Inspector) {
    
    /*
     * Visual representation of a connection between to models.
     */
    Inspector.View.ConnectionView = Marionette.ItemView.extend({
        
        /**
         *  Initializes the connectionview.
         *  
         * @param   {ConnectionModel]} connectionModel the model representing this connection
         * @param   {NodeView} fromNode        the view corresponding to the from StepModel
         * @param   {NodeView} toNode          the view corresponding to the to StepModel
         * @param   {object} svg             svg to draw on.
         */
        initialize: function (connectionModel, fromNode, toNode, svg) {
            var self = this;
            this.model = connectionModel;
            this.fromNode = fromNode;
            this.toNode = toNode;
            this.svg = svg;
            this.line = this.svg.append("line")
                .attr("marker-end", function (d) {
                    return "url(#arrow)";
                });
            
            this.closeCircle = this.svg.append('circle')
                .attr("class", "closeCircle")
                .attr('r', 10)
                .style("fill", "#000000");
            
            this.symbol = this.svg.append("text")
                .text('\uF00D')
                .attr("class", "close")
                .style("text-anchor", "middle");
            
            this.symbol.on('click', function(){
                self.remove();
            })

            this.model.on("destroy", this.destroy, this);
            this.updateConnection();

        },

        /**
         * Calculates the closest connectors for connecting the two nodes
         * @returns {Object} the two connectors which are closest.
         */
        getClosestConnectors: function () {
            
            // get the coordinates of the end node
            var x = this.toNode.getXCoordinate();
            var y = this.toNode.getYCoordinate();
            // Calculate the closest from connector
            var fromConnector = this.fromNode.getClosestConnector(x, y);

            // get the coordinates of the originating node
            x = this.fromNode.getXCoordinate();
            y = this.fromNode.getYCoordinate();
            // calculate the closest to connector.
            var toConnector = this.toNode.getClosestConnector(x, y);
            
            return {
                fromConnector: fromConnector,
                toConnector: toConnector
            }
        },
        
        /**
         * Set the SVG to draw on
         * @param {object} svg svg to draw on.
         */
        setSVG: function(svg){
            this.svg = svg;
        },
        
        /*
         * Forces reinitialisation of the connectionView
         */
        update: function(){
            this.line = this.svg.append("line")
                .attr("marker-end", function (d) {
                    return "url(#arrow)";
                });
            
            this.closeCircle = this.svg.append('circle')
                .attr("class", "closeCircle")
                .attr('r', 10)
                .style("fill", "#000000");
            
            this.symbol = this.svg.append("text")
                .text('\uF00D')
                .attr("class", "close")
                .style("text-anchor", "middle");
            
            this.symbol.on('click', function(){
                self.remove();
            })
            
            this.updateConnection();
        },

        /**
         * Updates the drawn connection when a NodeView is dragged.
         */
        updateConnection: function () {
            // get the closest connectors for each node
            var connectors = this.getClosestConnectors();
            
            // intermediate variables for readability
            var fromConn = connectors.fromConnector;
            var toConn = connectors.toConnector;
            
            // the begin coordinates of the line segment
            var x1 = this.fromNode.getXCoordinate() + parseInt(fromConn.attr("cx"));
            var y1 = this.fromNode.getYCoordinate() + parseInt(fromConn.attr("cy"));

            // end coordinates of the line segment
            var x2 = this.toNode.getXCoordinate() + parseInt(toConn.attr("cx"));
            var y2 = this.toNode.getYCoordinate() + parseInt(toConn.attr("cy"));

            // update the coordinates of the line
            this.line
                .attr("x1", x1)
                .attr("y1", y1)
                .attr("x2", x2)
                .attr("y2", y2);
            
            this.closeCircle
                .attr('cx', (x1 + x2) / 2)
                .attr('cy', (y1 + y2) / 2);
            
            this.symbol
                .attr('x', (x1 + x2) / 2)
                .attr('y', 5 + (y1 + y2) / 2);
            
                
        },
        
        /**
         * Removes this view completely, leave notraces.
         */
        remove: function(){
            this.fromNode.removeConnection(this);
            this.toNode.removeConnection(this);
            
            this.undelegateEvents();
            this.line.remove();
            this.closeCircle.remove();
            this.symbol.remove();

            connections = _.without(connections, this);
            this.model.destroy();
        },
        
        saveVisualisation: function(){
            var from = this.model.get("from");
            var to = this.model.get("to");
            
            return {
                from: from.cid,
                to: to.cid
            };
        }

    });


})(window.Inspector);