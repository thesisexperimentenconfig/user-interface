var nodes =  [];
var connections = [];

var modelValidator = require('experiment-validator');
var _ = require('lodash');

(function (Inspector) {
    
    /*
     * View for the graph representation of the experiment.
     */
    Inspector.View.GraphView = Inspector.View.BasicView.extend({

        template: "#graph-tpl",
        id: "graph",

        events: {
            'click #addNode': "showSelectorDialog"
        },

        initialize: function (options) {

            this.parent = options.parent;

            
            this.stepCollection = new Inspector.Collection.StepCollection();
            this.connectionCollection = new Inspector.Collection.ConnectionCollection();
            
            this.nodes = [];
            this.connections = [];
            
            this.isConnecting = false;

        },

        /**
         * Shows the Step selection menu
         */
        showSelectorDialog: function () {
            var self = this;
            var cb = function(step){
                self.addNode(step);
            };
            
            var keys = Object.keys(Inspector.StepModels);
            
            var selector = new Inspector.View.StepSelectorView({
                callback: cb,
                devices: keys
            }
            );
            //Show the view!
            selector.render();
            $("#graph").append(selector.el);
        },

        /**
         * Adds a new node to the graph based on a selected Step.
         * @param {String} selected string representing the selected step
         */
        addNode: function (selected) {
            var model = new Inspector.StepModels[selected]();
            this.stepCollection.add(model);

            var options = {
                svg: this.svg,
                parent: this
            }

            var nodeView = new Inspector.View.NodeView(model, options);
            this.nodes.push(nodeView);

            nodeView.render();

            this.showConfigurationView(model);

        },
        
        /**
         * Remove the given node from the graph
         */
        removeNode: function(node){
            this.nodes = _.without(this.nodes, node);
        },
        /**
         * Shows the view in which a user can configure a step.
         * @param {StepModel} model The model to be configured
         */
        showConfigurationView: function (model) {

            var configurationView = new Inspector.View.ConfigurationView(model);
            this.parent.showInConfig(configurationView);
        },

        /**
         * called when the graph is rendered.
         */
        onRender: function () {
            var _this = this;
            this.svg = d3.select("#graph").append("svg").attr("id", "g-canvas");

            this.svg.append("svg:defs").selectAll("marker")
                .data(["arrow"])
                .enter().append("svg:marker")
                .attr("id", String)
                .attr("viewBox", "0 -5 10 10")
                .attr("refX", 10)
                .attr("refY", 0)
                .attr("markerWidth", 6)
                .attr("markerHeight", 6)
                .attr("orient", "auto")
                .append("svg:path")
                .attr("d", "M0,-5L10,0L0,5");
        },
        
        /**
         * Redraws the nodes. Used to reinitialize the graph.
         */
        redrawNodes: function(){
            
            for(var i = 0; i < this.nodes.length; i++){
                this.nodes[i].setParent(this);
                this.nodes[i].setSVG(this.svg);
                this.nodes[i].render();
            }
            
            for(var j = 0; j < this.connections.length; j++){
                this.connections[j].setSVG(this.svg);
                this.connections[j].update();
            }
        },

        /**
         * Open a connection between two nodes
         * @param {NodeView} node The stard node from the connection
         */
        openConnection: function (node) {
            this.fromNode = node;
            this.isConnecting = true;
        },

        /**
         * Closes a connection between two nodes
         * @param {NodeView} node The end node from the connection
         */
        closeConnection: function (node) {
            this.isConnecting = false;

            if (this.fromNode === node) {
                smoke.alert("A device cannot be connected to itself");
                return;
            }
            
            var output = this.fromNode.getOutput();
            var input = node.getInput();
            
            if(!modelValidator.isValidConnection(output, input)){
                smoke.alert("I'm sorry, the two devices you selected cannot be connected. The output of the " + this.fromNode.getName() + " is not a valid input for the " + node.getName());
                return;
            }


            var connectionModel = new Inspector.Model.ConnectionModel({
                from: this.fromNode.getModel(),
                to: node.getModel()
            })

            var connectionView = new Inspector.View.ConnectionView(connectionModel, this.fromNode, node, this.svg);
            this.connectionCollection.add(connectionModel);
            
            if(!modelValidator.isAcyclicGraph(this.connectionCollection.toJSON())){
               smoke.alert("Cyclical experiments are not allowed!");
               this.connectionCollection.remove(connectionModel); 
               connectionModel.destroy();
            }else{
                this.connections.push(connectionView);
                node.addConnection(connectionView);
                this.fromNode.addConnection(connectionView);
                node.addConnection(connectionView); 
            }
            

        },
        
        /**
         * Save the current visualisation with models and connections to a JSON representation.
         * @returns {Object} [[Description]]
         */
        saveVisualisation: function(){
            var modelViews = [];
            for(var i = 0; i < this.nodes.length; i++){
                var modelView = this.nodes[i].saveVisualisation();
                modelViews.push(modelView);
            }
            
            var connectionsViews = [];
            for(var j = 0; j < this.connections.length; j++){
                var connectionView = this.connections[j].saveVisualisation();
                connectionsViews.push(connectionView);
            }
            
            return {
                models: modelViews,
                connections: connectionsViews
            }; 
        },
        
        /**
         * Load a new graph from a JSON representation.
         * @param {Object} representation the JSON representation of the graph
         */
        loadGraphFromJSON: function(representation){
            this.nodes = [];
            var models = representation.models;
            var store = {};
            var viewStore = {};
            for(var i = 0; i < models.length; i++){
                //var modeld = JSON.parse(models[i]);
                var model = new Inspector.StepModels[models[i].model.name](models[i].model);
               store[models[i].model.cid] = model;
                var options = {
                    svg: this.svg,
                    parent: this,
                    position: models[i].position
                };
                var nodeView = new Inspector.View.NodeView(model, options);
                this.nodes.push(nodeView);
                viewStore[models[i].model.cid] = nodeView;
                nodeView.render();
            }
            
            this.connections = [];
            
            var conns = representation.connections;
            for(var j = 0; j < conns.length; j++){
                var conn = conns[j];
                var fromId = conn.from;
                var toId = conn.to;
                
                var connectionModel = new Inspector.Model.ConnectionModel({
                    from: store[fromId],
                    to: store[toId]
                });
                
                this.connectionCollection.add(connectionModel);
               
                var fromView = viewStore[fromId];
                var toView = viewStore[toId];
                var connectionView = new Inspector.View.ConnectionView(connectionModel, fromView, toView, this.svg);
                
                this.connections.push(connectionView);
                
                viewStore[fromId].addConnection(connectionView);
                viewStore[toId].addConnection(connectionView);
                
               
            }
            
        },
        
        /**
         * Returns the view state of the graph. Can be used to load it later.
         * @returns {Object} ViewState
         */
        getViewState: function(){
            var viewState = {
                connections: this.connections,
                nodes: this.nodes,
                stepCollection: this.stepCollection,
                connectionCollection: this.connectionCollection
            };
            
            return viewState;
        },
        
        /**
          * Restores the viewstate of the view.
          * @param {object} viewState 
          */
        restoreViewState: function(viewState){
            this.connections = viewState.connections;
            this.nodes =viewState.nodes;
            this.stepCollection = viewState.stepCollection;
            this.connectionCollection = viewState.connectionCollection;
            this.redrawNodes();
    }
        

    });


})(window.Inspector);