(function(Inspector){
    /*
     * View that loads a template to show a fancy loader animation
     */
    Inspector.View.LoaderView = Backbone.Marionette.ItemView.extend({
        template: "#loader-tpl",
        id: "loader",
    })
})(window.Inspector);