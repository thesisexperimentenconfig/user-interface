(function(Inspector){
    
    /*
     * View that represents a dialog box to select a specific step to add to the experiment
     */
    Inspector.View.StepSelectorView = Backbone.Marionette.ItemView.extend({
        template: "#devicedialog-tpl",
        events:{
            "click #btn_device": "selectDevice"
        },
        
        /**
         * Initializes the view. Should be passaed a callback to execute
         * when a device is selected
         */
        initialize: function(options){
            this.callback = options.callback;
            this.model = new Backbone.Model({
                devices: options.devices
            });
        },
        
        /**
         * Executes when a device has been selected in the view.
         * The selected value is passed to the callback
         */
        selectDevice: function(){
            var selected = $("#deviceselect").find(":selected").val();
            this.callback(selected);
            this.destroy();
        }
        
    });
    
})(window.Inspector);