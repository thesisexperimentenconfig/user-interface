var sequenceChecker = require('sequence-checker');
var fileHandler = require('filehandler');
var XMLExporter = require('experiment-xml-exporter');

(function (Inspector) {

    Inspector.View.PipelineView = Marionette.CompositeView.extend({
        template: "#pipeline-tpl",
        childViewContainer: "tbody#sequences",

        events: {
            "click #addsequence": 'addSequence',
            "click #addByAccessionId": "addByAccessionId",
            "click #selectCurrentExperiment": "selectCurrentExperiment",
            'click #simpope': 'process'

        },
        id: "pipeline",

        initialize: function () {
            this.collection = new Inspector.Collection.SequenceCollection();
            this.childView = Inspector.View.SequenceView;
        },
        
        /**
         * Adds a sequence based on the user input
         */
        addSequence: function () {

            var sequence = $("#sequence").val();

            if (!sequence) {
                return alert("Empty Sequence not allowed");
            }

            if (!this.checkUniqueSequence(sequence)) {
                return alert("Sequence was already added!");
            }

            if (sequenceChecker.isValidSequence(sequence)) {
                $("#noSeq").remove();
                this.collection.add(new Inspector.Model.SequenceModel({
                    sequence: sequence
                }));
            } else {
                alert('not a valid sequence!');
            }

            $("#sequence").val('');

        },
        
        /**
         * Adds a sequence based on an Accession ID.
         */
        addByAccessionId: function () {
            var accessionId = $("#accessionid").val();
            var sequence = sequenceChecker.getSequenceFromAccessionID(accessionId);
            try {
                if (this.checkUniqueSequence(sequence)) {
                    $("#noSeq").remove();
                    this.collection.add(new Inspector.Model.SequenceModel({
                        sequence: sequence
                    }));
                } else {
                    alert("The Protein Sequence you entered has already been added!");
                }
            } catch (e) {
                alert("No sequence found with the given Accession Id");
            }


        },

        /**
         * Checks whether a given sequence was already added to this view.
         * @param   {String}  seq1 sequence to check.
         * @returns {Boolean} 
         */
        checkUniqueSequence: function (seq1) {
            var sequences = this.collection.models;
            for (var i = 0; i < sequences.length; i++) {
                var seq2 = sequences[0].get("sequence");
                if (seq1.toUpperCase() === seq2.toUpperCase()) return false;
            }
            return true;
        },

        /**
         * Select a experiment on which the analysis should be performed.
         */
        selectCurrentExperiment: function () {
            var self = this;
            $('body').append('<input type="file" id="pipelineFile" accept=".simp" style="display:none">');
            $("#pipelineFile").click();
            $("#pipelineFile").on("change", function () {
                var filename = $("#pipelineFile").get(0).files[0].path;
                var data = fileHandler.openExperimentFile(filename);
                self.experiment = self.transformData(data);
                self.showExperimentMetaData();

            });
        },
        
        /**
         * Transforms the json data to a more suitable format
         */
        transformData: function(data){
            var json = JSON.parse(data);
            
            var models = [];
            if(!!json.models){
                for(var i = 0; i < json.models.length; i++){
                    models.push(json.models[i].model);
                }
            }
            
            json.models = models;
            
            return json;
            
        },

        /**
         * Shows the metadata of the experiment in the view.
         */
        showExperimentMetaData: function () {
            if (!!this.experiment) {
                $('#experimentMetadata').append('<div>Number of Models: ' + this.experiment.models.length + '</div>');
                $('#experimentMetadata').append('<div>Number of connections: ' + this.experiment.connections.length + '</div>');
            }

        },
        
        /**
         * Checks whether the used has added protein sequences
         * @returns {Boolean} 
         */
        hasProteins: function () {
            return this.collection.models.length > 0;
        },

        /**
         * Processes the users input to show results
         */
        process: function () {

            if (!this.hasProteins()) {
                return alert('You need to add proteins!');
            }

            if (!this.experiment) {
                return alert('You have to select an experiment');
            }
            $('#results').show();
            this.showLoader();

            var self = this;
            var dataToConvert = {
                models: this.experiment.models,
                connections:this.experiment.connections,
                sequences: this.collection.toJSON()
            };
            
            var xmlRepresentation = XMLExporter.getExperimentRepresentation(dataToConvert);
            console.log(xmlRepresentation);
            
            var results = this.getResults();
            var resultModel = new Backbone.Model({
                results: results
            }); 
            var resultView = new Inspector.View.ResultView({
                model: resultModel
            });

            resultView.render();
            
            setTimeout(function () {
                self.removeLoader();
                $('#result').empty();
                $("#result").append(resultView.el);

            }, 2000);
        },
        
        /**
         * THIS METHOD RETURNS DUMMY DATA FOR NOW
         */
        getResults: function(file){
            var data = require('./assets/dummydata');
            return data;
        },

        /**
         * Shows a loader animation
         */
        showLoader: function () {

            var loaderView = new Inspector.View.LoaderView();
            loaderView.render();
            var html = loaderView.el;
            $("#result").append(html);
        },

        /**
         * Removes the loader animation
         */
        removeLoader: function () {
            $("#loader").remove();
        },

        /**
          * Retrieves the view state of the view.
          */
        getViewState: function () {
            return {
                sequences: this.collection,
                experiment: {}
            };
        },

        /**
          * Restores the viewstate of the view.
          * @param {object} viewState 
          */
        restoreViewState: function (viewState) {
            this.collection = viewState.sequences;
            this.render();
            $("#noSeq").remove();
        }

    });
})(window.Inspector);