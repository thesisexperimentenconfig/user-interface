(function(Inspector){
    
    /**
     * View used to show the results which have been retrieved from simpope
     * needs a model that contains a key with a results array attached.
     * example: 
     * 
     * var resultModel = new Backbone.Model({
     *          results: [{
     *              rank: 1,
     *              sequence: 'GVIVESA',
     *              Q1: 690.38,
     *              Q3: 656.36,
     *              RT: 34.3,
     *              probability: 0.94,
     *              intensity: 0.32
     *              }]
     *      })
     * 
     */
    Inspector.View.ResultView = Marionette.ItemView.extend({
        template: "#result-tpl"
    });
})(window.Inspector);