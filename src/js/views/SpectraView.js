
var path = require('path');
var spectraFileOpener = require('inspector-msconverter');
(function (Inspector) {

    Inspector.View.SpectraView = Inspector.View.BasicView.extend({
        template: "#spectra-tpl",
        id: "spectraview",

        events: {
            "click .list-entry": "changeSelected"
        },

        initialize: function (options) {
            if (options) {
                this.model = options.model;
                this.experiment = options.model.get("experiment");
            }
            
            this.selected = 0;
        },
        
        /**
         * Opens the spectra from file
         * @param   {String} filenameThe file to be opend
         */
        openFile: function (filename) {
            
            var self = this;
            this.showLoader();
            
            spectraFileOpener.openFile(filename, function (err, experiment) {
                if(err){
                    return smoke.alert('Oops, could not open file!');
                }
                
                self.removeLoader();
                self.experiment = experiment;
                self.showGraph();
                self.showList();
            });
        },
        
        /**
         * Shows loader animation
         */
        showLoader: function(){
            var loaderView = new Inspector.View.LoaderView();
            loaderView.render();
            var html = loaderView.el;
            $("#spectra").append(html);
        },
        
        /**
         * Removes loader animation
         */
        removeLoader: function(){
             $("#loader").remove();
        },

        /**
         * Shows the graph on the screen.
         */
        showGraph: function () {
            var data = this.experiment[this.selected];
            var width = $("#spectra").innerWidth();
            var height = $("#spectra").innerHeight() - 100;
            if (!this.svg) {
                this.svg = d3.select("#spectra").append("svg").attr("class", "spectra-graph");
            }

            this.svg.selectAll("*").remove();


            var maxValues = data.maxValues;
            var minValues = data.minValues;

            var xAxisScale = this.createXAxis(minValues, maxValues, width, height);
            var yAxisScale = this.createYAxis(minValues, maxValues, width, height);

            this.drawPeaks(data, height, width, xAxisScale, yAxisScale);


            
        },
        
         zoomed: function() {
                svg.select(".x.axis").call(xAxis);
                svg.select(".y.axis").call(yAxis);
            },

        createYAxis: function (minValues, maxValues, width, height) {
            var yAxisScale = d3.scale.linear()
                .domain([0, maxValues.maxIntensity * 1.1])
                .range([height, 100]);

            var yAxis = d3.svg.axis()
                .scale(yAxisScale)
                .orient("left")
                .innerTickSize(-width + 200)
                .outerTickSize(5)
                .tickPadding(10);
            this.svg.append("g")
                .attr("class", "y axis")
                .attr("transform", "translate(100,0)")
                .call(yAxis);

            return yAxisScale;
        },

        createXAxis: function (minValues, maxValues, width, height) {
            var xAxisScale = d3.scale.linear()
                .domain([minValues.minMZ, maxValues.maxMZ])
                .range([100, width - 100]);
            var xAxis = d3.svg.axis().scale(xAxisScale)
                .orient("bottom")
                .innerTickSize(-height + 100)
                .outerTickSize(5)
                .tickPadding(10);

            this.svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + (height) + ")")
                .call(xAxis);

            return xAxisScale;
        },
        
        drawPeaks: function (data, height, width, xAxisScale, yAxisScale) {
            var _this = this;


            var _this = this
            this.svg.selectAll(".back")
                .data(data.peaks)
                .enter()
                .append("rect")
                .attr('width', 1)
                .attr("fill", "red")
                .attr("class", function (d, i) {
                    return "highlight-" + i;
                })
                .attr("fill-opacity", 0)
                .attr("x", function (point) {
                    return xAxisScale(point[0]);
                })
                .attr("height", function (point) {
                    return height - 100;
                })
                .attr("y", function (point) {
                    return 100;
                }).on("mouseover", function (point, i) {
                    d3.select(this).attr("fill-opacity", 1);
                    $(".top-" + i).attr("fill-opacity", 1);
                    _this.showPeak(point);
                }).on("mouseout", function (d, i) {
                    d3.select(this).attr("fill-opacity", 0);
                    $(".top-" + i).attr("fill-opacity", 0);
                });



            var rects = this.svg.selectAll(".dot")
                .data(data.peaks)
                .enter()
                .append("rect")
                .attr("width", 1)
                .attr("fill", "#000000")
                .attr("x", function (point) {
                    return xAxisScale(point[0]);
                })
                .attr("height", function (point) {
                    return height - yAxisScale(point[1]);
                })
                .attr("y", function (point) {
                    return yAxisScale(point[1]);
                }).on("mouseover", function (d, i) {
                    $(".highlight-" + i).attr("fill-opacity", 1);
                    $(".top-" + i).attr("fill-opacity", 1);
                    _this.showPeak(d);
                }).on("mouseout", function (d, i) {
                    $(".highlight-" + i).attr("fill-opacity", 0);
                    $(".top-" + i).attr("fill-opacity", 0);
                });


            var circ = this.svg.selectAll(".circ")
                .data(data.peaks)
                .enter()
                .append("circle")
                .attr('r', 4)
                .attr("cx", function (point) {
                    return xAxisScale(point[0]);
                }).attr("cy", function (point) {
                    return yAxisScale(point[1]);
                })
                .attr("fill", "blue")
                .attr("fill-opacity", "0")
                .attr("class", function (d, i) {
                    return "top-" + i;
                });
        },

        showPeak: function (point) {
            $("#mz").text(point[0]);
            $("#intensity").text(point[1]);
        },

        showList: function () {
            var table = $("#spectra-table");
            for (var i = 0; i < this.experiment.length; i++) {
                var html = "<tr class='list-entry' data-id=" + i + "><td data-id=" + i + "> Spectrum " + i + "</td><td data-id=" + i + "> " + this.experiment[i].RT + "</td></tr>";
                table.append(html);
            }
        },

        changeSelected: function (event) {
            var id = $(event.target).data("id");
            this.selected = parseInt(id);
            this.showGraph();
        },
        
        getViewState: function(){
            return {
                experiment: this.experiment
            };
         },
         
         /**
          * Restores the viewstate of the view.
          */
         restoreViewState: function(viewState){
             this.showLoader();
             this.experiment = viewState.experiment;
             this.showGraph();
             this.showList();
             this.removeLoader();
         }



    })
})(window.Inspector);