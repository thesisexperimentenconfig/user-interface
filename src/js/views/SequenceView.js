(function (Inspector) {
    /**
     * View representing a Sequence. Requires a SequenceModel.
     */
    Inspector.View.SequenceView = Backbone.Marionette.ItemView.extend({
        template: "#sequence-tpl",
        tagName: "tr",
        events: {
            "click .destroy": "deleteSequence"
        },
        
        /**
         * deletes the sequence
         */
        deleteSequence: function () {
            this.model.destroy();
            this.destroy();
        }
    })
})(window.Inspector);