var modelValidator = require('experiment-validator');

(function(Inspector){
    
    /*
     * BasicView to be used in the application.
     * Forces extending Models to absolutely include required functionality.
     */
     Inspector.View.BasicView  = Backbone.Marionette.ItemView.extend({
       
         /**
          * Retrieves the view state of the view.
          */
         getViewState: function(){
            throw "This needs to be implemented in Views that inherit of the BasicView";
         },
         
         /**
          * Restores the viewstate of the view.
          * @param {object} viewState 
          */
         restoreViewState: function(viewState){
             throw "This needs to be implemented in Views that inherit of the BasicView";
         }
    });        
    
})(window.Inspector);