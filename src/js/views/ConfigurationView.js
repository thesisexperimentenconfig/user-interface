var modelValidator = require('experiment-validator');
var _ = require('lodash');

(function(Inspector){
    
    /*
     * View which contains the form to configure a step in an experiment.
     * Handles saving changes to the corresponding model and marks invalid parameters.
     */
    Inspector.View.ConfigurationView =  Marionette.ItemView.extend({
        template: "#config-tpl",
        id: "conf",
        
        events:{
            "click #save_config": "save"
        },
        
        initialize: function(model, options){
            this.model = model;
            // if the model is destroyed, the view should also be destroyed.
            this.model.on("destroy", this.destroy, this);
            
            
        },
        
        /*
         * Marks which parameters are invalid when the view is shown
         */
        onShow: function(){
            this.showInvalidFields();
            if(this.model.has("parameters")){
                var parameters = this.model.get('parameters');
                parameters = JSON.stringify(parameters);
                if(parameters === '{}'){
                    $("#save_config").hide();
                }
            
            
            }
        },
        
        /*
         * Saves the fileld in parameters to the model.
         */
        save: function(){
            if(!this.model.has("parameters")){
                return;
            }
            
            var parameters = this.model.get("parameters");

            var newParameters = _.cloneDeep(parameters);
            for(var parameter in parameters){
                var value = this.$("#" + parameter).val();
                newParameters[parameter].value = value;
            }

            this.showInvalidFields();
            this.model.set("parameters", newParameters);
            this.model.trigger("change"); 
        },
        
        /*
         * Shows the invalid fields of the model when trying to save.
         */
        showInvalidFields: function(){
            if(!this.model.has("parameters")){
                return;
            }
            
            var parameters = this.model.get("parameters");
            for(parameter in parameters){
                if(modelValidator.isValidParameter(parameters[parameter])){
                    $("#" + parameter).removeClass('invalid-field');
                } else{
                    $("#" + parameter).addClass('invalid-field');
                }
            }

        },
        
        /*
         * destroys the view entirely, can be necessary to prevent unwanted side-effects.
         */
        destroyView: function() {
            
            //COMPLETELY UNBIND THE VIEW
            this.undelegateEvents();
            
            $(this.el).removeData().unbind(); 

            //Remove view from DOM
            this.remove();  
            Backbone.View.prototype.remove.call(this);

        }

    });
           
    
})(window.Inspector);