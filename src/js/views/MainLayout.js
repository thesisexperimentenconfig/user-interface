(function(Inspector){
    'use strict';
    /*
     * Layout used for the application. Used to load other views/ layouts.
     */
    Inspector.Layout.MainLayout = Backbone.Marionette.LayoutView.extend({
        
        template: '#main-tpl',
        
        id: "wrapper",
        
        regions:{
            Navigation: "#navigation",
            Main: "#main-area"
            
        },
        
        onShow: function(){
            this.Navigation.show(new Inspector.View.NavigationView());

        }
    });

})(window.Inspector);