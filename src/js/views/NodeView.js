
var modelValidator = require('experiment-validator');

(function (Inspector) {

    Inspector.View.NodeView = Backbone.Marionette.ItemView.extend({

        initialize: function (model, options) {
            // The transformation model which this node represents
            this.model = model;
            // An svg is needed to be able to draw the nodes
            if (options.svg) {
                this.svg = options.svg;
            } else {
                console.error("Cannot initialize node view, the svg container is not defined!");
            }
            
            if(!options.position){
                // set a random initial position for the node
                this.initializePosition(this.model);
            }else{
                // use input for the initial position of the node
                this.transX = options.position.x;
                this.transY = options.position.y;
            }
            

            if (options.parent) {
                this.parent = options.parent;
            }

            this.width = 220;
            this.height = 125;
            this.active = true;
            this.titleHeight = 35;
            this.fontSize = 12;
            this.connectorRadius = 7;
            this.model.on('change', this.redrawParameters, this);
            this.connectors = [];
            this.connections = [];

        },

        /**
         * If the model has a position, initialize it, otherwise, a random value is added.
         */
        initializePosition: function (model) {
            if (!model.get('position') || model.get('position') === '') {
                this.transX = 50 +  Math.random() * 100;
                this.transY = 50 + Math.random() * 100;
            }else{
                var position = model.get('position');
                this.transX = position[0];
                this.transY = position[1];
            }

        },
        
        /**
         * returns a function that implements the drag behaviour of the node
         */
        getDragFunction: function(){
            var self = this;

            var drag = d3.behavior.drag()
                .on("drag", function () {
                    self.transX += d3.event.dx;
                    self.transY += d3.event.dy;
                    d3.select(this)
                        .attr('x', self.transX)
                        .attr('y', self.transY);
                    if (self.connections) {
                        self.updateConnections();
                    }
                });
            
            return drag;
        },
        
        /**
         * Updates the connections to and from this node.
         */
        updateConnections: function(){
            for(var i = 0; i < this.connections.length; i++){
                this.connections[i].setSVG(this.svg);
                this.connections[i].updateConnection();
            }
        },
        
        
        
        /**
         * Renders this NodeView.
         */
        render: function () {
            var drag = this.getDragFunction();
            
            // We use an svg object instead of a g - element, 
            //because svg objects can be positioned without transform.
            this.container = this.svg.append("svg");
            this.container.call(drag);

            this.container.attr('x', this.transX)
                .attr('y', this.transY);

            // draw the main box and the titlebox.
            this.drawBox(this.connectorRadius, this.connectorRadius, this.width, this.height, "main-box");
            this.title = this.drawBox(this.connectorRadius, this.connectorRadius, this.width, this.titleHeight, "title-box");
            this.statusSymbol = this.drawText(this.width - 10, this.height - 3, "valid-symbol", '\uf058');
                   
            
            
            this.drawText(15, 28, 'title-box-text', this.model.get("name"));

            // draw the connectors
            this.drawConnector(this.connectorRadius, this.connectorRadius + this.height / 2);
            this.drawConnector(this.connectorRadius + this.width, this.connectorRadius + this.height / 2);
            this.drawConnector(this.connectorRadius + this.width / 2, this.connectorRadius);
            this.drawConnector(this.connectorRadius + this.width / 2, this.connectorRadius + this.height);

            this.container.on("click", function (d) {
                d3.event.stopPropagation();
                that.parent.showConfigurationView(that.model); 

            });
            
            var that = this;
            var closeSymbol = this.drawText(205, 26, "close", '\uF00D');
            closeSymbol.on("click", function () {
                d3.event.stopPropagation();
                that.remove();
            });
            
            
            this.redrawParameters();

        },
        
        /**
         * Draws a box on the screen
         * @param   {Number} x        x coordinate
         * @param   {Number} y        y coordinate
         * @param   {Number} width    
         * @param   {Number} height   
         * @param   {string} cssClass
         * @returns {Object} the drawn box.
         */
        drawBox: function (x, y, width, height, cssClass) {
            var box = this.container.append("rect")
                .attr('class', cssClass)
                .attr('x', x)
                .attr('y', y)
                .attr('width', width)
                .attr('height', height);

            return box;
        },
        
        /**
         * Draws the parameters for the model visualized by this nodeview.
         */
        drawParameters: function () {
            //start with enough padding
            var y = this.titleHeight + 20;
            var lineHeight = this.fontSize * 1.5;

            if (this.model.get("parameters")) {

                var parameters = this.model.get("parameters");
                for (var i in parameters) {
                    this.drawText(15, y, "body-text", parameters[i].name);

                    if (parameters[i].value) {
                        this.drawText(150, y, "body-text b", parameters[i].value);
                    }
                    
                    if(parameters[i].unit){
                        this.drawText(this.width - 30, y, "body-text b", parameters[i].unit);
                    }

                    y += lineHeight;

                    if (y >= this.height - lineHeight) {
                        this.drawText(15, y, "body-text", "...");
                        break;
                    }
                }
            } else {
                this.drawText(15, y, "body-text", "No parameters");
            }

        },

        /**
         * Draws a connector where a new connection can be initialized or ended.
         * @param {Number} cx x coordinate for connector
         * @param {Number} cy y coordinate for connector
         */
        drawConnector: function (cx, cy) {
            var _this = this;
            var connector = this.container.append("circle")
                .attr('class', 'connector')
                .attr('cx', cx)
                .attr('cy', cy)
                .attr('r', this.connectorRadius)
                .on("click", function () {
                    d3.event.stopPropagation();
                    _this.clickConnectorHandler(connector);


                });
            this.connectors.push(connector);
        },
        
        /**
         * Draw text.
         * @param   {[[Type]]} x        x coordinate
         * @param   {[[Type]]} y        y coordinate
         * @param   {[[Type]]} cssClass [[Description]]
         * @param   {[[Type]]} text     the text to draw
         * @returns {[[Type]]} the drawn text object.
         */
        drawText: function (x, y, cssClass, text) {
            var text = this.container.append("text")
                .text(text)
                .attr("class", cssClass)
                .attr('x', x)
                .attr('y', y);

            return text;
        },


        /**
         * Redraws the parameters
         */
        redrawParameters: function () {
            this.container.selectAll(".body-text").remove();
            this.drawParameters();
            
            if(!modelValidator.isValidModel(this.model.toJSON())){
                this.statusSymbol.attr('class',"invalid-symbol");
                this.statusSymbol.text( '\uf06a');
                this.title.attr('class', 'invalid-title-box')
            }else{
                this.statusSymbol.attr('class',"valid-symbol");
                this.statusSymbol.text('\uf058');
                this.title.attr('class','title-box');
            }   
            
        },
        
        /**
         * Set the svg to draw on 
         * @param {[[Type]]} svg [[Description]]
         */
        setSVG: function(svg){
            this.svg = svg;
        },
        
        /**
         * Function that handles the opening/ closing of a connection
         * @param {[[Type]]} connector the clickec connector
         */
        clickConnectorHandler: function (connector) {
            if (this.parent.isConnecting) {
                this.parent.closeConnection(this, connector);
            } else {
                this.parent.openConnection(this, connector);
            }


        },

        /**
         * Removes and destroys this node.
         */
        remove: function() {
            nodes = _.without(nodes, this);
            
            this.removeConnections();
            this.undelegateEvents();
            this.container.remove();
            this.parent.removeNode(this);
            this.model.destroy();
        },

        /**
         * removes all connections to this node.
         */
        removeConnections: function () {
            for (var c in this.connections) {
                var connection = this.connections[c];
                if (!!connection && connection.toNode !== null) {
                    connection.toNode.removeConnection(connection);
                }

                if (!!connection && connection.fromNode !== null) {
                    connection.fromNode.removeConnection(connection);
                }
                
                if(!!connection){
                    connection.remove();
                }


            }
        },

        /*
         * remove a connection
         */
        removeConnection: function (connection) {
            this.connections = _.without(this.connections, connection);
            connection.line.remove();
        },

        /*
         * Returns the connector closest to the given x and y coordinates from this node.
         */
        getClosestConnector: function (x, y) {

            var closestConnector = null;
            var minDistance = Number.POSITIVE_INFINITY;

            for (var c in this.connectors) {

                var connector = this.connectors[c];

                var cx = parseInt(connector.attr("cx")) + this.getXCoordinate();
                var cy = parseInt(connector.attr("cy")) + this.getYCoordinate();
                var difX = Math.abs(x - cx); 
                var difY = Math.abs(y - cy);
                var distance = difX * difX + difY * difY;
                if (distance <= minDistance) {
                    closestConnector = connector;
                    minDistance = distance;
                }
            }

            return closestConnector;
        },
        
        /**
         * Save this visualisation to a JSON representation
         * @returns {Object} json representation
         */
        saveVisualisation: function(){
            var jsonModel = this.model.toJSON();
            jsonModel.cid = this.model.cid;
    
            var position = {
                x: this.transX,
                y: this.transY
            };
            
            return {
                model: jsonModel,
                position: position
            };
            
        },
        
        /**
         * Set the parent
         */
        setParent: function(parent){
            this.parent = parent;
        },
        
        /**
         * set the connections
         * @param {[ConnectionView]} connections connections to use
         */
        setConnections: function(connections){
          this.connections = connections;  
        },

        /**
         * Add a connection
         * @param {ConnectionView} connection connection to add
         */
        addConnection: function (connection) {
            this.connections.push(connection);
        },
        
        
        /**
         * returns the model this view represents
         */
        getModel: function(){
            return this.model;
        },
        
        /**
         * returns the valid output
         * @returns {String} 
         */
        getOutput: function () {
            return this.model.get("output");
        },
        
        /**
         * returns the valid input
         * @returns {String} 
         */
        getInput: function(){
            return this.model.get("input");
        },
        
        getName: function(){
            return this.model.get('name');
        },

        // return the X coordinate of this node
        getXCoordinate: function () {
            return this.transX;
        },

        // return the Y coordinate of this node
        getYCoordinate: function () {
            return this.transY;
        },

        // returns the width of the node
        getWidth: function () {
            return this.width;
        },

        // returns the width of the node
        getHeight: function () {
            return this.height;
        }
    });

})(window.Inspector);