var _ = require('lodash');

(function (Inspector) {
    'use strict';

    Inspector.StepModel = Backbone.Model.extend({
        
        initialize: function(){
            this.defaults = _.clone(this.description);
            var attrs = _.defaults({}, attrs, _.result(this, 'defaults'));
            var options = {};
            this.set(attrs, options);
            this.set("cid", this.cid);
        }
    });


})(window.Inspector);