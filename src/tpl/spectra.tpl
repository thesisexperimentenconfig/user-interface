<div id="spectra">
   <div class="head">Mass Spectrum Chart</div>
    <table id="measurements">
        <thead>
            <tr>
                <th>Measurement</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
            
            <td class="measurement">M/Z </td><td id="mz"> 0 </td>
        </tr>
        <tr>
            <td class="measurement">Intensity </td><td id="intensity"> 0</td>
        </tr>
        </tbody>
    </table>
    <svg id="spectra-graph"></svg>
</div>
<div id="spectra-list">
    <div class="head">Mass spectra</div>
<table>
    <thead>
        <th>Index</th>
        <th>Retention Time</th>
    </thead>
    <tbody id='spectra-table'>
        
    </tbody>
</table>
</div>