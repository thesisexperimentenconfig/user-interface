<div id="pipeline-config">

    <div class="sec">
        <div class="head">Select experiment</div>
        <div class="bod">
            <div id="experimentMetadata"></div>
            <button id="selectCurrentExperiment">Select experiment from file</button>
        </div>

    </div>

    <div class="sec">
        <div class="head">Enter Sequence</div>
        <div class="bod">
            <textarea name="sequence" id="sequence"></textarea>
            <button id="addsequence"> Add Sequence</button>

        </div>
    </div>
    
    <div class="sec">
        <div class="head">Enter Accession Id</div>
        <div class="bod">
            <input type="text" id="accessionid" placeholder="Enter Accession Id">
            <button id="addByAccessionId">Add Sequence with Accession Id</button>
        </div>
    </div>

</div>

<div id="proteins">
   <div class="sec" id="results" style="display:none">
        <div class="head">Results from SIMPOPE</div>
        <div class="bod" id="result">
            
        </div>
   </div>
    <div class="sec">

        <div class="protein-list">
            <div class="head">Sequence list</div>
            <div class="bod">
                <table>
                    <thead>
                        <tr>
                            <th class="sequence">Sequence</th>
                            <th></th>
                        </tr>

                    </thead>
                    <tbody id="sequences">
                        <tr id="noSeq"><td>none</td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="sec">
        <button id="simpope">Process with SIMPOPE</button>
    </div>
</div>