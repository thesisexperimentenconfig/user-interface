<div class="dialog">
    <div class="title">Select a device</div>
    <div class="select">
        <select name="deviceselect" id="deviceselect">
        
        {{#each devices}}
        <option value="{{this}}">{{this}}</option>
        {{/each}}
        </select>
        <button class="but"id="btn_device">Add Device</button>
    </div>
</div>