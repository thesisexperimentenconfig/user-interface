<div id="programname">SIMPOPE</div>
   <div id="controls">
    
    <div id="windowControls">
        <div id="minimizeWindow" class="windowControl">
        </div>
        <div id="maximizeWindow" class="windowControl">
        </div>
        <div id="closeWindow" class="windowControl">
        </div>
    </div>
</div>
<ul class="navigation">
    <li><a href="#experiment-nav" class="active">Model Experiment</a>
        <ul id="experiment-nav">
            <a href="#currentmodel">Current Experiment</a>
            <a href="#openmodel">Open experiment</a>
            <a href="#model">New experiment</a>
            <a href="#savemodel">Save experiment</a>
        </ul>

    </li>
    
        
    <li> <a href="#visualisation-nav">Visualize spectra</a>
    <ul id="visualisation-nav">
            <a href="#currentMassSpectra" style="display:none">Current Mass Spectra</a>
            <a href="#openMassSpectra">Open Mass spectra file</a>
        </ul>
    </li>
    
        

    <li><a href="#pipeline">Analyze with SIMPOPE</a>
    </li>
</ul>