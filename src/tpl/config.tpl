<div class="head">Configure {{name}}</div>
<div class="config-wrap">
{{#if description}}
<div class="desc">{{description}}</div>
{{/if}}
{{#if input}}
<div class="desc">Input: {{#each input}} {{this}} <br>{{/each}} </div>
{{/if}}
{{#if output}}
<div class="desc">Output: {{#each output}} {{this}} <br>{{/each}} </div>
{{/if}}
 {{#if parameters}} {{#each parameters}}

    <label for="{{name}}">{{name}} {{#if unit}} ({{{unit}}})
    {{/if}} </label>
    <span style="color: #999999">{{#if min}} {{min}} &le; {{/if}}
    {{name}} 
    {{#if max}} &le; {{max}}  {{/if}}</span>
    {{#if enum}}
    <select name="" id="{{@key}}" >
        {{#each enum}}
        <option value="{{this}}">{{this}}</option>
        {{/each}}
    </select>
    {{else}}
    <input type="text" name="{{@key}}" id="{{@key}}" value=
    {{#if value}}{{value}}{{/if}}>

    {{/if}}
{{/each}}
<button class="but" id="save_config">Save Configuration</button>
{{else}}
<div class="message">No parameters to configure...</div>
{{/if}}
</div>