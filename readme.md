# Dependencies

This is an overview of the dependencies required to run the application inside the **user-interface** folder.

- _**MongoDB 2.6.4**_: download from https://www.mongodb.org/downloads [download from https://www.mongodb.org/downloads](download from https://www.mongodb.org/downloads)
- _**OpenMS 1.11.1**_: you can download and install it from this url [http://sourceforge.net/projects/open-ms/files/OpenMS/OpenMS-1.11.1/](http://sourceforge.net/projects/open-ms/files/OpenMS/OpenMS-1.11.1/)
- _**Python 2.7**_: Install via package manager or from [https://www.python.org/](https://www.python.org/)
- _**Numpy**_: this can be installed via the package manager `sudo apt-get install python-numpy`, for windows, you can use the version of numpy that is provided here: [http://www.lfd.uci.edu/~gohlke/pythonlibs/#numpy](http://www.lfd.uci.edu/~gohlke/pythonlibs/#numpy)
- _**PyOpenMS 1.11**_: Python bindings for the openMS library. Follow the installation instruction on [https://pypi.python.org/pypi/pyopenms/1.11](https://pypi.python.org/pypi/pyopenms/1.11)
- **_Node.js 0.12.0_** Download and install it from [https://nodejs.org/](https://nodejs.org/). Comes with the package manager NPM.
- **Node-webkit 12.1** Download the correct version for your platform on: [http://dl.nwjs.io/v0.12.1/](http://dl.nwjs.io/v0.12.1/)

# Running the application
After you have downloaded **Node-webkit**, direct your terminal to the **user-interface** folder. In this folder, run `npm install`. This will take care of other required dependencies. Afterwards, extract the downloaded **Node-webkit**  in this folder. You can run the application by running `./nw`.

The application can be tested with the files supplied in `/test_files`.